<?php

namespace Davek1312\Console;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputDefinition;
use Davek1312\VariableUtils\StringUtils;
use InvalidArgumentException;

/**
 * Generates the command name and arrays of InputOption and InputArgument from the command signature
 *
 * @author David Kelly <davek1312@gmail.com>
 */
class InputSignatureParser {

    /**
     * @var string
     */
    private $signature;
    /**
     * @var array
     */
    private $arguments = [];
    /**
     * @var array
     */
    private $options = [];

    /**
     * @param string $signature
     */
    public function __construct($signature) {
        $this->signature = trim($signature);
        $this->generateArgumentsAndOptions();
    }

    /**
     * Creates an InputDefinition from the command signature
     *
     * @return InputDefinition
     */
    public function getInputDefinition() {
        return new InputDefinition(array_merge($this->options, $this->arguments));
    }

    /**
     * Parses the command name from the command signature
     *
     * @return string
     *
     * @throws InvalidArgumentException
     */
    public function getName() {
        if($this->signature == '' || !preg_match('/[^\s]+/', $this->signature, $parts)) {
            throw new InvalidArgumentException('Command name is empty.');
        }
        return $parts[0];
    }

    /**
     * Sets $arguments and $options
     *
     * @return void
     *
     * @throws InvalidArgumentException
     */
    private function generateArgumentsAndOptions() {
        $inputs = $this->getInputs();
        foreach($inputs as $input) {
            if($input == '') {
                throw new InvalidArgumentException('Input is empty.');
            }
            if($this->isOption($input)) {
                $this->options[] = $this->getOption($input);
            }
            else {
                $this->arguments[] = $this->getArgument($input);
            }
        }
    }

    /**
     * Return an array of inputs from the command signature
     *
     * @return array
     */
    private function getInputs() {
        preg_match_all('/\{\s*(.*?)\s*\}/', $this->signature, $parts);
        return $parts[1];
    }

    /**
     * @param string $input
     *
     * @return boolean
     */
    private function isOption($input) {
        return StringUtils::startsWith($input, '--');
    }

    /**
     * Create an InputOption from the input
     *
     * @param string $input
     *
     * @return InputOption
     */
    private function getOption($input) {
        $input = str_replace('--', '', $input);
        $name = $this->getInputName($input);
        $default = $this->getInputDefaultValue($name);
        $inputOption = new InputOption($this->getOptionName($name, $default), $this->getOptionShortcut($name), $this->getOptionMode($name, $default), $this->getInputDescription($input), $default);
        return $inputOption;
    }

    /**
     * Parses the option name
     *
     * @param string $name
     * @param string $default
     *
     * @return string
     */
    private function getOptionName($name, $default) {
        $optionShortcutAndName = $this->getOptionShortcutAndName($name);
        $optionName = count($optionShortcutAndName) == 2 ? $optionShortcutAndName[1] : $optionShortcutAndName[0];
        return str_replace(['=', '*', $default], '', $optionName);
    }

    /**
     * Parses the option shortcut
     *
     * @param string $name
     *
     * @return string|null
     */
    private function getOptionShortcut($name) {
        $optionShortcutAndName = $this->getOptionShortcutAndName($name);
        return count($optionShortcutAndName) == 2 ? $optionShortcutAndName[0] : null;
    }

    /**
     * Parses the option shortcut and name. Shortcut is index 0 and name is index 1
     *
     * @param string $name
     *
     * @return array
     */
    private function getOptionShortcutAndName($name) {
        return preg_split('/\s*\|\s*/', $name, 2);
    }

    /**
     * Parses the option's default value
     *
     * @param string $name
     *
     * @return string|null
     */
    private function getInputDefaultValue($name) {
        if(!$this->isOptionValueArray($name)) {
            if(preg_match('/(.+)\=(.+)/', $name, $parts)) {
                return $parts[2];
            }
        }
        return null;
    }

    /**
     * Parses the option mode
     *
     * @param string $name
     * @param string $default
     *
     * @return integer
     */
    private function getOptionMode($name, $default) {
        switch(true) {
            case StringUtils::endsWith($name, '=') || ($default != null):
                return InputOption::VALUE_OPTIONAL;
            case $this->isOptionValueArray($name):
                return InputOption::VALUE_OPTIONAL + InputOption::VALUE_IS_ARRAY;
            default:
                return InputOption::VALUE_NONE;
        }
    }

    /**
     * Determines if the option input is an array
     *
     * @param string $name
     *
     * @return boolean
     */
    private function isOptionValueArray($name) {
        return StringUtils::endsWith($name, '=*');
    }

    /**
     * Create an InputArgument from the input
     *
     * @param string $input
     *
     * @return InputArgument
     */
    private function getArgument($input) {
        $name = $this->getInputName($input);
        $default = $this->getInputDefaultValue($name);
        $inputArgument = new InputArgument($this->getArgumentName($name, $default), $this->getArgumentMode($name, $default),  $this->getInputDescription($input), $default);
        return $inputArgument;
    }

    /**
     * Parses the argument name
     *
     * @param string $name
     * @param string $default
     *
     * @return string
     */
    private function getArgumentName($name, $default) {
        return str_replace(['=', '*', '?', $default], '', $name);
    }

    /**
     * Parses the option mode
     *
     * @param string $name
     * @param string $default
     *
     * @return integer
     */
    private function getArgumentMode($name, $default) {
        switch(true) {
            case StringUtils::endsWith($name, '?') || ($default != null):
                return InputArgument::OPTIONAL;
            case StringUtils::endsWith($name, '*'):
                return InputArgument::REQUIRED + InputArgument::IS_ARRAY;
            case StringUtils::endsWith($name, '?*'):
                return InputArgument::IS_ARRAY;
            default:
                return InputArgument::REQUIRED;
        }
    }

    /**
     * Parses the input name
     *
     * @param string $input
     *
     * @return string
     */
    private function getInputName($input) {
        $inputNameAndDescription = $this->getInputNameAndDescription($input);
        return $inputNameAndDescription[0];
    }

    /**
     * Parses the input description
     *
     * @param string $input
     *
     * @return string|null
     */
    private function getInputDescription($input) {
        $inputNameAndDescription = $this->getInputNameAndDescription($input);
        return count($inputNameAndDescription) == 2 ? $inputNameAndDescription[1] : null;
    }


    /**
     * Parses the input name and description. Name is index 0 and description is index 1
     *
     * @param string $input
     *
     * @return array
     */
    private function getInputNameAndDescription($input) {
        return preg_split('/\s+:\s+/', $input, 2);
    }

    /**
     * @return string
     */
    public function getSignature() {
        return $this->signature;
    }

    /**
     * @param string $signature
     */
    public function setSignature($signature) {
        $this->signature = $signature;
    }

    /**
     * @return array
     */
    public function getArguments() {
        return $this->arguments;
    }

    /**
     * @param array $arguments
     */
    public function setArguments($arguments) {
        $this->arguments = $arguments;
    }

    /**
     * @return array
     */
    public function getOptions() {
        return $this->options;
    }

    /**
     * @param array $options
     */
    public function setOptions($options) {
        $this->options = $options;
    }
}