<?php

namespace Davek1312\Console;

use Davek1312\App\App;
use Symfony\Component\Console\Application as SymfonyApplication;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;

/**
 * Wrapper class for Symfony\Component\Console\Application class
 *
 * @author  David Kelly <davek1312@gmail.com>
 */
class Application extends SymfonyApplication {

    /**
     * Registers the application's commands
     *
     * @return Application
     */
    public static function registerCommands() {
        $application = new static();
        $commands = static::getCommands();
        if($commands) {
            foreach($commands as $command) {
                $application->add(new $command);
            }
        }
        return $application;
    }

    /**
     * Gets the commands from the config file
     *
     * @return array
     */
    private static function getCommands() {
        $app = new App();
        return $app->getCommands();
    }

    /**
     * Executes the command for the given name and input
     *
     * @param string $command
     * @param array $input
     *
     * @return integer
     */
    public static function call($command, array $input = []) {
        $application = static::registerCommands();
        $application->setAutoExit(false);
        $application->setCatchExceptions(false);
        $input = array_merge([
            'command' => $command
        ], $input);
        return $application->run(new ArrayInput($input), new BufferedOutput());
    }
}