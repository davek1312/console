<?php

namespace Davek1312\Console;

use Symfony\Component\Console\Command\Command as SymfonyCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;

/**
 * Wrapper class for Symfony\Component\Console\Command\Command class
 *
 * @author  David Kelly <davek1312@gmail.com>
 */
abstract class Command extends SymfonyCommand {

    /**
     * @var string
     */
    protected $signature;
    /**
     * @var string
     */
    protected $description;
    /**
     * @var string
     */
    protected $help;

    /**
     * @var InputSignatureParser
     */
    private $inputSignatureParser;
    /**
     * @var InputInterface
     */
    private $input;
    /**
     * @var OutputInterface
     */
    private $output;

    /**
     * Command constructor. Sets $inputSignatureParser
     */
    public function __construct() {
        parent::__construct();
        $this->setDefinition($this->getInputSignatureParser()->getInputDefinition());
    }

    /**
     * Sets the command's $signature, $description, $help
     *
     * @return void
     */
    protected function configure() {
        $this->setName($this->getInputSignatureParser()->getName());
        $this->setDescription($this->description);
        $this->setHelp($this->help);
    }

    /**
     * Execute the command
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output) {
        $this->input = $input;
        $this->output = $output;
        $this->process();
    }

    /**
     * Checks if the argument is present
     *
     * @param string $name
     *
     * @return boolean
     */
    public function hasArgument($name) {
        return $this->input && $this->input->hasArgument($name);
    }

    /**
     * Return the value for the argument name
     *
     * @param string $name
     *
     * @return string|array|null
     */
    public function getArgument($name) {
        return $this->input ? $this->input->getArgument($name) : null;
    }

    /**
     * Checks if the option is present
     *
     * @param string $name
     *
     * @return boolean
     */
    public function hasOption($name) {
        return $this->input && $this->input->hasOption($name);
    }

    /**
     * Write to console output
     *
     * @param string $message
     * @param string $style
     *
     * @return void
     */
    protected function outputLine($message, $style = null) {
        $this->output->writeln($this->formatOutputMessage($message, $style));
    }

    /**
     * Write to console output with info style
     *
     * @param string $message
     *
     * @return void
     */
    protected function outputInfo($message) {
        $this->outputLine($message, 'info');
    }

    /**
     * Write to console output with comment style
     *
     * @param string $message
     *
     * @return void
     */
    protected function outputComment($message) {
        $this->outputLine($message, 'comment');
    }

    /**
     * Write to console output with question style
     *
     * @param string $message
     *
     * @return void
     */
    protected function outputQuestion($message) {
        $this->outputLine($message, 'question');
    }

    /**
     * Write to console output with error style
     *
     * @param string $message
     *
     * @return void
     */
    protected function outputError($message) {
        $this->outputLine($message, 'error');
    }

    /**
     * Formats the output method to add a style
     *
     * @param string $message
     * @param string $style
     *
     * @return string
     */
    protected function formatOutputMessage($message, $style) {
        return $style ? "<$style>$message</$style>" : $message;
    }

    /**
     * Return the value for the option name
     *
     * @param string $name
     *
     * @return string|array|null
     */
    public function getOption($name) {
        return $this->input ? $this->input->getOption($name) : null;
    }

    /**
     * Executes the command for the given name and input
     *
     * @param string $command
     * @param array $input
     *
     * @return integer
     */
    public function call($command, array $input) {
        return Application::call($command, $input);
    }

    /**
     * Create and return a ProgressBar
     *
     * @param integer $count
     *
     * @return ProgressBar
     */
    public function getProgressBar($count) {
        $progressBar = new ProgressBar($this->output, $count);
        $progressBar->start();
        return $progressBar;
    }

    /**
     * Prompts the user to confirm a message
     *
     * @param string $message
     * @param boolean $default
     *
     * @return boolean
     */
    public function inputConfirm($message, $default = false) {
        $question = new ConfirmationQuestion($message, $default);
        return $this->askQuestion($question);
    }

    /**
     * Prompts the user for a value from a message
     *
     * @param string $message
     * @param string $default
     *
     * @return string
     */
    public function inputAsk($message, $default = null) {
        $question = new Question($message, $default);
        return $this->askQuestion($question);
    }

    /**
     * Prompts the user for a value from a message and a list of predefined answers
     *
     * @param string $message
     * @param array $choices
     * @param integer|array $default
     * @param boolean $multiSelect
     * @param integer $maxAttempts
     * @param string $errorMessage
     *
     * @return string|array
     */
    public function inputAskChoice($message, array $choices, $default = null, $multiSelect = false, $maxAttempts = null, $errorMessage = null) {
        $question = new ChoiceQuestion($message, $choices, $default);
        $question->setMultiselect($multiSelect);
        $question->setMaxAttempts($maxAttempts);
        $question->setErrorMessage($errorMessage);
        return $this->askQuestion($question);
    }

    /**
     * Prompts the user for a value and autocompletes their response from a list of potential anserts
     *
     * @param string $message
     * @param array $potentialChoices
     * @param string $default
     *
     * @return string
     */
    public function inputAskAutocomplete($message, array $potentialChoices, $default = null) {
        $question = new Question($message, $default);
        $question->setAutocompleterValues($potentialChoices);
        return $this->askQuestion($question);
    }

    /**
     * Prompts the user for a value and also hides their response from the console
     *
     * @param string $message
     * @param string $default
     *
     * @return string
     */
    public function inputAskHidden($message, $default = null) {
        $question = new Question($message, $default);
        $question->setHidden(true);
        $question->setHiddenFallback(false);
        return $this->askQuestion($question);
    }

    /**
     * Create a helper and ask $question
     *
     * @param Question $question
     *
     * @return mixed
     */
    public function askQuestion(Question $question) {
        $helper = $this->getHelper('question');
        return $helper->ask($this->input, $this->output, $question);
    }

    /**
     * @return string
     */
    public function getSignature() {
        return $this->signature;
    }

    /**
     * @param string $signature
     */
    public function setSignature($signature) {
        $this->signature = $signature;
    }

    /**
     * If $inputSignatureParser is empty one will be created
     *
     * @return InputSignatureParser
     */
    public function getInputSignatureParser() {
        if(!$this->inputSignatureParser) {
            $this->inputSignatureParser = new InputSignatureParser($this->signature);
        }
        return $this->inputSignatureParser;
    }

    /**
     * @param InputSignatureParser $inputSignatureParser
     */
    public function setInputSignatureParser($inputSignatureParser) {
        $this->inputSignatureParser = $inputSignatureParser;
    }

    /**
     * @return InputInterface
     */
    public function getInput() {
        return $this->input;
    }

    /**
     * @param InputInterface $input
     */
    public function setInput($input) {
        $this->input = $input;
    }

    /**
     * @return OutputInterface
     */
    public function getOutput() {
        return $this->output;
    }

    /**
     * @param OutputInterface $output
     */
    public function setOutput($output) {
        $this->output = $output;
    }

    /**
     * This is where the user adds their command logic
     *
     * @return void
     */
    abstract protected function process();
}