<?php

namespace Davek1312\Console\Tests;

use Davek1312\Console\InputSignatureParser;
use InvalidArgumentException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use ReflectionClass;

class InputSignatureParserTest extends \PHPUnit_Framework_TestCase {

    public function testGetName() {
        $this->assertSignatureInvalid('');
        $this->assertSignatureInvalid(null);
        $this->assertSignatureInvalid('   ');
        $this->assertSignatureValid(' {a} {b}', '{a}');
        $this->assertSignatureValid('name {a} {b}', 'name');
        $this->assertSignatureValid('name', 'name');
    }

    private function assertSignatureInvalid($signature) {
        try {
            $inputSignatureParser = new InputSignatureParser($signature);
            $inputSignatureParser->getName();
            $this->assertFalse(true);
        }
        catch(InvalidArgumentException $e) {
            $this->assertSame('Command name is empty.', $e->getMessage());
        }
    }

    private function assertSignatureValid($signature, $name) {
        $inputSignatureParser = new InputSignatureParser($signature);
        $this->assertSame($name, $inputSignatureParser->getName());
    }

    public function testGetOptions() {
        $this->assertInputInvalid('name {}', 'Input is empty.');

        $inputSignatureParser = new InputSignatureParser('name {argument} {--option} {--option : description} {--O|option} {--option=} {--option=*} {--option=default} {--O|option=default : description}');
        $options = $inputSignatureParser->getOptions();
        $this->assertCount(7, $options);
        $this->assertOptionEquals($options[0], 'option');
        $this->assertOptionEquals($options[1], 'option', null, null, 'description');
        $this->assertOptionEquals($options[2], 'option', 'O');
        $this->assertOptionEquals($options[3], 'option', null, InputOption::VALUE_OPTIONAL);
        $this->assertOptionEquals($options[4], 'option', null, InputOption::VALUE_OPTIONAL + InputOption::VALUE_IS_ARRAY);
        $this->assertOptionEquals($options[5], 'option', null, InputOption::VALUE_OPTIONAL, '', 'default');
        $this->assertOptionEquals($options[6], 'option', 'O', InputOption::VALUE_OPTIONAL, 'description', 'default');
    }

    private function assertInputInvalid($signature, $error) {
        try {
            new InputSignatureParser($signature);
            $this->assertFalse(true);
        }
        catch(InvalidArgumentException $e) {
            $this->assertSame($error, $e->getMessage());
        }
    }

    private function assertOptionEquals($option, $name, $shortcut = null, $mode = null, $description = '', $default = null) {
        $this->assertTrue($option->equals(new InputOption($name, $shortcut, $mode, $description, $default)));
        $this->assertEquals($description, $option->getDescription());
    }

    public function testGetArguments() {
        $inputSignatureParser = new InputSignatureParser('name {argument} {argument : description} {argument=default} {argument*} {argument*?} {argument=default : description} {--option}');
        $arguments = $inputSignatureParser->getArguments();
        $this->assertCount(6, $arguments);
        $this->assertArgumentEquals($arguments[0], 'argument', InputArgument::REQUIRED);
        $this->assertArgumentEquals($arguments[1], 'argument', InputArgument::REQUIRED, 'description');
        $this->assertArgumentEquals($arguments[2], 'argument', InputArgument::OPTIONAL, '', 'default');
        $this->assertArgumentEquals($arguments[3], 'argument', InputArgument::REQUIRED + InputArgument::IS_ARRAY, '', []);
        $this->assertArgumentEquals($arguments[4], 'argument', InputArgument::OPTIONAL);
        $this->assertArgumentEquals($arguments[5], 'argument', InputArgument::OPTIONAL, 'description', 'default');
    }

    private function assertArgumentEquals($argument, $name, $mode, $description = '', $default = null) {
        $this->assertEquals($name, $argument->getName());
        $this->assertEquals($description, $argument->getDescription());
        $this->assertEquals($default, $argument->getDefault());

        //No getter in InputArgument
        $reflectionClass = new ReflectionClass(InputArgument::class);
        $reflectionProperty = $reflectionClass->getProperty('mode');
        $reflectionProperty->setAccessible(true);
        $this->assertEquals($mode, $reflectionProperty->getValue($argument));
    }

    public function testGetInputDefinition() {
        $inputSignatureParser = new InputSignatureParser('name {argument} {--option}');
        $inputDefinition = $inputSignatureParser->getInputDefinition();
        $this->assertTrue($inputDefinition->hasArgument('argument'));
        $this->assertTrue($inputDefinition->hasOption('option'));
    }
}