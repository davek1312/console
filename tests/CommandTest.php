<?php

namespace Davek1312\Console\Tests;

use Davek1312\Console\Tests\Mock\TestCommand;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Tester\CommandTester;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Input\ArgvInput;

class CommandTest extends \PHPUnit\Framework\TestCase {

    private $testCommand;

    public function setUp() {
        parent::setUp();
        $this->testCommand = new TestCommand();
    }

    public function testGetInputSignatureParser() {
        $inputSignatureParser = $this->testCommand->getInputSignatureParser();
        $this->assertEquals($inputSignatureParser->getSignature(), $this->testCommand->getSignature());
    }

    public function test__construct() {
        $inputSignatureParser = $this->testCommand->getInputSignatureParser();
        $this->assertEquals($inputSignatureParser->getInputDefinition(), $this->testCommand->getDefinition());
        $this->assertCommandConfigure();
    }

    private function assertCommandConfigure() {
        $inputSignatureParser = $this->testCommand->getInputSignatureParser();
        $this->assertEquals($inputSignatureParser->getName(), $this->testCommand->getName());
        $this->assertEquals('description', $this->testCommand->getDescription());
        $this->assertEquals('help', $this->testCommand->getHelp());
    }

    public function testHasArgument() {
        $command = $this->getTestCommand();
        $this->assertFalse($command->hasArgument('argument'));
        $command = $this->addArgumentToCommand($command, 'argument');
        $this->assertTrue($command->hasArgument('argument'));
    }

    public function testGetArgument() {
        $command = $this->getTestCommand();
        $this->assertNull($command->getArgument('argument'));
        $command = $this->addArgumentToCommand($command, 'argument');
        $this->assertEquals($command->getArgument('argument'), 'value');
    }

    public function testHasOption() {
        $command = $this->getTestCommand();
        $this->assertFalse($command->hasOption('option'));
        $command = $this->addOptionToCommand($command, 'option');
        $this->assertTrue($command->hasOption('option'));
    }

    public function testGetOption() {
        $command = $this->getTestCommand();
        $this->assertNull($command->getOption('option'));
        $command = $this->addOptionToCommand($command, 'option');
        $this->assertEquals($command->getOption('option'), 'value');
    }

    private function getTestCommand() {
        return static::getCommand(TestCommand::class, 'console:test');
    }

    private function addArgumentToCommand($command, $name, $value = 'value') {
        $input = new ArgvInput();
        $input->bind($command->getDefinition());
        $input->setArgument($name, $value);
        $command->setInput($input);
        return $command;
    }

    private function addOptionToCommand($command, $name, $value = 'value') {
        $input = new ArgvInput();
        $input->bind($command->getDefinition());
        $input->setOption($name, $value);
        $command->setInput($input);
        return $command;
    }

    public function testCall() {
        $this->assertEquals(0, $this->testCommand->call('console:test', [
            'argument' => 'value'
        ]));
    }

    public function testCommandExecuteAndProcess() {
        $this->assertContains('argument=argument', static::getTestCommandOutput());
    }

    public function testOutputLine() {
        $this->assertContains('argument=argument', static::getTestCommandOutput());
    }

    public function testOutputInfo() {
        $this->assertContains('info', static::getTestCommandOutput());
    }

    public function testOutputComment() {
        $this->assertContains('comment', static::getTestCommandOutput());
    }

    public function testOutputQuestion() {
        $this->assertContains('question', static::getTestCommandOutput());
    }

    public function testOutputError() {
        $this->assertContains('error', static::getTestCommandOutput());
    }

    private function getTestCommandOutput($options = [], $inputs = []) {
        $argumentsAndOptions = array_merge([
            'argument' => 'argument',
        ], $options);
        $command = static::getCommand(TestCommand::class, 'console:test');
        return static::getCommandOutput($command, $argumentsAndOptions, $inputs);
    }

    public function testGetProgressBar() {
        $command = $this->getTestCommand();
        $command->setOutput(new ConsoleOutput());
        $max = 5;
        $progressBar = $command->getProgressBar($max);
        $this->assertEquals($max, $progressBar->getMaxSteps());
    }

    public function testInputConfirm() {
        $this->assertQuestionAnswered('--input_confirm', ['yes'], 'confirm?confirmed');
    }

    public function testInputAsk() {
        $this->assertQuestionAnswered('--input_ask', ['answer'], 'ask?answer');
    }

    public function testInputAskChoice() {
        $this->assertQuestionAnswered('--ask_choice', ['only_answer'], 'ask_choice?');
        $this->assertQuestionAnswered('--ask_choice', ['only_answer'], 'only_answeranswered');
    }

    private function assertQuestionAnswered($optionName, array $inputs, $outputShouldContain) {
        $output = $this->getTestCommandOutput( [
            $optionName => true,
        ], $inputs);
        $this->assertContains($outputShouldContain, $output);
    }

    public function testInputAskAutocomplete() {
        $this->assertQuestionAnswered('--ask_autocomplete', ['autocomplete_answer'], 'ask_autocomplete?');
    }

    public function testInputAskHidden() {
        $this->assertQuestionAnswered('--ask_hidden', ['hidden_answer'], 'ask_hidden?hidden_answer');
    }

    public static function getCommand($commandClass, $commandName) {
        $application = new Application();
        $application->add(new $commandClass());
        return $application->find($commandName);
    }

    public static function getCommandOutput($command, $argumentsAndOptions = [], array $inputs = []) {
        $commandTester = new CommandTester($command);
        $commandTester->setInputs($inputs);
        $commandTester->execute(array_merge([
            'command' => $command->getName(),
        ], $argumentsAndOptions));
        return $commandTester->getDisplay();
    }
}