<?php

namespace Davek1312\Console\Tests\Bin;

class Davek1312ConsoleTest extends \PHPUnit\Framework\TestCase {

    const COMMAND_FILE = 'bin/davek1312-console';

    public function testCommandsRegistered() {
        exec(static::getCommand().' list', $output, $returnValue);
        $this->assertContains('  console:test  description', $output);
    }

    public static function getCommand() {
        return 'php '.__DIR__.'/../../'.static::COMMAND_FILE;
    }
}