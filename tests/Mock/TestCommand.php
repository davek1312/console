<?php

namespace Davek1312\Console\Tests\Mock;

use Davek1312\Console\Command;

class TestCommand extends Command {

    protected $signature = 'console:test {argument} {--option=} {--input_confirm} {--input_ask} {--ask_choice} {--ask_autocomplete} {--ask_hidden}';
    protected $description = 'description';
    protected $help = 'help';

    protected function process() {
        $this->outputLine('argument='.$this->getArgument('argument').',option='.$this->getOption('option'));
        $this->outputInfo('info');
        $this->outputComment('comment');
        $this->outputQuestion('question');
        $this->outputError('error');

        $count = 3;
        $progressBar = $this->getProgressBar($count);
        for($i = 0; $i < $count; $i++) {
            $progressBar->advance();
        }
        $progressBar->finish();

        if($this->getOption('input_confirm')) {
            if($this->inputConfirm('confirm?')) {
                $this->outputLine('confirmed');
            }
        }

        if($this->getOption('input_ask')) {
            $this->outputLine($this->inputAsk('ask?'));
        }

        if($this->getOption('ask_choice')) {
            $this->outputLine($this->inputAskChoice('ask_choice?', ['only_answer']).'answered');
        }

        if($this->getOption('ask_autocomplete')) {
            $this->outputLine($this->inputAskAutocomplete('ask_autocomplete?', ['autocomplete_answer']));
        }

        if($this->getOption('ask_hidden')) {
            $this->outputLine($this->inputAsk('ask_hidden?'));
        }
    }
}