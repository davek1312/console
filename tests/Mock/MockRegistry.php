<?php

namespace Davek1312\Console\Tests\Mock;

class MockRegistry extends \Davek1312\App\Registry {

    protected function register() {
        $this->registerCommands('Davek1312\Console\Tests\Mock\TestCommand');
    }
}