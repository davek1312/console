<?php

namespace Davek1312\Console\Tests;

use Davek1312\App\App;
use Davek1312\Console\Application;

class ApplicationTest extends \PHPUnit_Framework_TestCase {

    public function testRegisterCommands() {
        $application = Application::registerCommands();
        $app = new App();
        $commands = $app->getCommands();
        $registeredCommands = $application->all();
        foreach($commands as $command) {
            $command = new $command();
            $this->assertArrayHasKey($command->getName(), $registeredCommands);
        }
    }

    public function testCall() {
        $this->assertEquals(0, Application::call('console:test', [
            'argument' => 'value'
        ]));
    }
}