# davek1312/console

Wrapper for the symfony/console package.

# Installation

The package is available on [Packagist](https://packagist.org/packages/davek1312/console),
you can install it using [Composer](https://getcomposer.org/).

```bash
composer require davek1312/console
```

# Creating Commands
Create your command class and implement `process()`:
```php
<?php

namespace MyApp\Commands;

use Davek1312\Console\Command;

class MyCommand extends Command {

    protected $signature = 'myapp:mycommand {argument} {--option=}';
    protected $description = 'My command\'s short description';
    protected $help = 'My command\'s full description';

    protected function process() {
        // MyCommand's code logic
    }
}
```

# Command Inputs
The `signature` property defines the command's arguments and options.
```php
   /**
    * This command expects one argument for name and with option with a value for age.
    */
    protected $signature = 'myapp:mycommand {name} {--age=}';
```

## Arguments
```php
    // Required argument
    myapp:mycommand {name}
    Console useage: vendor/bin/davek1312-console myapp:mycommand David
    
    // Argument with a description
    myapp:mycommand {name : Name's description}
   
    // Optional argument
    myapp:mycommand {name?}
    Console useage: vendor/bin/davek1312-console myapp:mycommand
   
    // Argument with a default value
    myapp:mycommand {name=David}
    
    // Argument where the value is an array
    myapp:mycommand {name*}
    Console useage: vendor/bin/davek1312-console myapp:mycommand David Paul
    
    // Optional argument where the value is an array
    myapp:mycommand {name?*}
```

## Options
```php
    // Option where no value is expected. The value is when not present and true when present.
    myapp:mycommand {--age}
    Console useage: vendor/bin/davek1312-console myapp:mycommand --age
    
    // Option with a description.
    myapp:mycommand {--age : description}
    
    // Option with a command line shortcut
    myapp:mycommand {--A|age}
    Console useage: vendor/bin/davek1312-console myapp:mycommand --A=20
   
    // Option where a value is expected
    myapp:mycommand {--age=}
    Console useage: vendor/bin/davek1312-console myapp:mycommand --age=20    
   
    // Option with a default value
    myapp:mycommand {--age=100}
    
    // Option where the value is an array
    myapp:mycommand {--age=*}
    Console useage: vendor/bin/davek1312-console myapp:mycommand --age=20 --age=30
```

## Registering Commands
To register your commands view the [davek1312\app](https://packagist.org/packages/davek1312/app) documentation.

# Calling Commands
## Command Line
```bash
vendor/bin/davek1312-console myapp:mycommand argument-value --option=option-value
```

## Programmatically
Running `call` in both examples returns an integer return code.

### From Another Command
```php
$this->call('myapp:mycommand', [
    'argument' => 'argument-value', 
    '--option' => 'option-value'
]);
```

### From Anywhere Else In Your Application
```php
Davek1312\Console\Application::call('myapp:mycommand', [
    'argument' => 'argument-value', 
    '--option' => 'option-value'
]);
```

# Console Input/Output

## Prompting For Input
There a few ways to ask the user for input:
```php
// Ask the user for confirmation
$confirm = $this->inputConfirm('Do you want to continue? [y/n]');

// Ask the user for a value
$name = $this->inputAsk('What is your name?', $default);

// Ask the user for a value from a list of predefined values
$name = $this->inputAskChoice('What is your name?', ['David', 'Paul']);

// Ask the user for a value and their response will be autocompleted from the list of values provided
$name = $this->inputAskAutocomplete('What is your name?', ['David', 'Paul']);

// Ask the user for a value but hide their response as they typre
$password = $this->inputAskHidden('What is your password?');
```

## Writing Output
To write to the console output you can use the following methods:
```php
// "Message" is displayed in default style
$this->outputLine('Message');

// "Message" is displayed in green text
$this->outputInfo('Message');

// "Message" is displayed in yellow text
$this->outputComment('Message');

// "Message" is displayed with text in a blue background
$this->outputQuestion('Message');

// "Message" is displayed with text in a red background
$this->outputError('Message');
```

## Progress Bar
To display the progress of the command you can supply the `ProgressBar` with a count:
```php
$count = 3;
$progressBar = $this->getProgressBar($count);
for($i = 0; $i < $count; $i++) {
    //Do something
    $progressBar->advance();
}
$progressBar->finish();
```

# Testing Commands
You can execute and obtain the console output of commands using the following code:
```php
<?php

use Davek1312\Console\Tests\CommandTest;

$command = CommandTest::getCommand(MyCommand::class,'myapp:mycommand');
/**
 * $argumentsAndOptions are the commands defined arguments and options
 * $inputs is an array of booleans that answer i/o inputs
 */
CommandTest::getCommandOutput($command, $argumentsAndOptions, $inputs);
```